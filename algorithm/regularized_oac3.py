# coding=utf-8
"""Regularized OAC3 algorithm implementation."""
import logging
import math

import numpy

import simplex_projection
from algorithm import oac3

DEFAULT_STEP = 0.1


class RegularizedOAC3(oac3.OAC3):
    """RegularizedOAC3 algorithm implementation."""

    def __init__(self, num_classes, alpha=oac3.DEFAULT_ALPHA,
                 beta=oac3.DEFAULT_BETA, lambd=oac3.DEFAULT_LAMBDA,
                 step=DEFAULT_STEP, divergence=oac3.DEFAULT_DIVERGENCE):
        """Constructor of WeightedOAC3."""
        super(RegularizedOAC3, self).__init__(
            num_classes, alpha=alpha, beta=beta, lambd=lambd,
            divergence=divergence)
        # Parameters to build the model
        self._step = step

    def _apply_regularization(self, estimates):
        """Apply regularization over these estimates."""
        closest_class = []
        new_estimates = numpy.copy(estimates)
        for one_class in range(self._num_classes):
            current_closest = -1
            current_divergence = -1
            for other_class in range(self._num_classes):
                if other_class == one_class:
                    continue  # Do not compare class with itself
                diver = sum(
                    (estimates[one][one_class] - estimates[one][other_class])
                    ** 2 for one in range(self._num_samples))
                if current_divergence == -1 or diver < current_divergence:
                    current_divergence = diver
                    current_closest = other_class
            closest_class.append(current_closest)
        step = self._step / math.sqrt(self._current_iteration)
        for one_class in range(self._num_classes):
            if closest_class[one_class] == -1:
                # No closest class, so skip it!
                continue
            for one in range(self._num_samples):
                new_estimates[one][one_class] -= step * (
                    estimates[one][closest_class[one_class]] -
                    estimates[one][one_class])
        return new_estimates

    def _regularize_all(self):
        """Apply regularization over all current estimates."""
        logging.debug('Applying regularization')
        self._current_left = self._apply_regularization(self._current_left)
        self._current_right = self._apply_regularization(self._current_right)

    def _project_to_simplex(self):
        """Project current estimates into |u|_1 = 1."""
        for one in range(self._num_samples):
            self._current_right[one] = simplex_projection.proj_simplex(
                self._current_right[one])
            self._current_left[one] = simplex_projection.proj_simplex(
                self._current_left[one])

    def _inner_loop(self):
        """One iteration of the algorithm, to be extended."""
        self._iterate_right()
        self._iterate_left()
        self._regularize_all()
        self._project_to_simplex()
