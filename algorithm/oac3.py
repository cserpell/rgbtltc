# coding=utf-8
"""OAC3 (Acharya, 2014) algorithm implementation."""
import logging

import numpy

from algorithm import base

SQUARED_EUCLIDEAN = 'squared_euclidean'
GENERALIZED_I = 'generalized_i'

DEFAULT_ALPHA = 0.1
DEFAULT_BETA = 0.1
DEFAULT_LAMBDA = 0.1
DEFAULT_DIVERGENCE = SQUARED_EUCLIDEAN


class OAC3(base.Algorithm):
    """OAC3 (Acharya, 2014) algorithm implementation."""

    def __init__(self, num_classes, alpha=DEFAULT_ALPHA, beta=DEFAULT_BETA,
                 lambd=DEFAULT_LAMBDA, divergence=DEFAULT_DIVERGENCE):
        """Constructor of OAC3."""
        super(OAC3, self).__init__()
        # Parameters to build the model
        self._num_classes = num_classes
        self._alpha = alpha
        self._beta = beta
        self._lambda = lambd
        self._divergence = divergence
        self._the_ones = None
        # Divergences gradients and inverse gradients
        self._gradient, self._inv_gradient, self._hessian, self._inv_hessian = self._gradients(
            divergence)
        # Model information required for predictions
        self._num_samples = None
        self._num_labeled = None
        self._current_iteration = None
        # Required by this algorithm
        self._total_sim_from = None
        self._non_zero_from = None
        self._similarity_matrix = None
        self._average_estimates = None
        self._source_models = None
        self._cluster_models = None
        self._labeled_labels = None
        self._current_right = None
        self._current_left = None

    def _gradients(self, divergence_name):
        if divergence_name == SQUARED_EUCLIDEAN:
            return (lambda x: 2.0 * x,
                    lambda x: 0.5 * x,
                    lambda x: 2.0 * numpy.identity(x.shape[0]),
                    lambda x: 0.5 * numpy.identity(x.shape[0]))
        if divergence_name == GENERALIZED_I:
            self._the_ones = numpy.ones(self._num_classes)
            return (lambda x: numpy.log(x) + self._the_ones,
                    lambda x: numpy.exp(x - self._the_ones),
                    lambda x: numpy.diag(numpy.power(x, -1)),
                    numpy.diag)
        return None, None

    def _build_similarity_matrix(self):
        self._similarity_matrix = numpy.zeros(
            (self._num_samples, self._num_samples))
        num_models = len(
            self._cluster_models[0]) if len(self._cluster_models) else 0
        self._non_zero_from = []
        for one in range(self._num_samples):
            current_non_zero = []
            for two in range(self._num_samples):
                total = 0
                for cluster in range(num_models):
                    if (self._cluster_models[one][cluster] ==
                            self._cluster_models[two][cluster]):
                        total += 1
                similarity = float(total) / num_models
                if similarity < 0.1:
                    similarity = 0.
                else:
                    current_non_zero.append(two)
                # Everywhere we use similarity matrix is multiplied by alpha
                # so fix it here
                self._similarity_matrix[one][two] = self._alpha * similarity
                self._similarity_matrix[two][one] = self._alpha * similarity
            self._non_zero_from.append(current_non_zero)
        # Fixed already with parameters to avoid computation afterwards
        self._total_sim_from = [self._lambda +
                                sum(self._similarity_matrix[one][two]
                                    for two in self._non_zero_from[one])
                                for one in range(self._num_samples)]

    def _build_average_estimates(self):
        num_models = len(
            self._source_models[0]) if len(self._source_models) else 0.
        self._average_estimates = [
            sum(self._source_models[one][model]
                for model in range(num_models)) / num_models
            for one in range(self._num_samples)]

    def _base_estimate(self, one):
        return self._average_estimates[one]

    def _iterate_right(self):
        for one in range(self._num_samples):
            num = (
                self._base_estimate(one) +
                sum(self._similarity_matrix[one][two] * self._current_left[two]
                    for two in self._non_zero_from[one]) +
                self._lambda * self._current_left[one])
            den = 1.0 + self._total_sim_from[one]
            # Assuming first part of samples are labeled ones
            if one < self._num_labeled:
                num += self._beta * self._labeled_labels[one]
                den += self._beta
            self._current_right[one] = num / den

    def _iterate_left(self):
        gradients = [self._gradient(self._current_right[one])
                     for one in range(self._num_samples)]
        for one in range(self._num_samples):
            num = (sum(self._similarity_matrix[one][two] * gradients[two]
                       for two in self._non_zero_from[one]) +
                   self._lambda * gradients[one])
            self._current_left[one] = self._inv_gradient(
                num / self._total_sim_from[one])

    def start(self, labeled_labels, source_models, cluster_models):
        """Configure algorithm class with global variables."""
        logging.debug('Configuring algorithm...')
        self._current_iteration = 0
        self._num_labeled = len(labeled_labels)
        self._num_samples = len(source_models)
        self._source_models = source_models
        self._cluster_models = cluster_models
        self._labeled_labels = labeled_labels
        self._build_similarity_matrix()
        self._current_right = numpy.ones(
            (self._num_samples, self._num_classes)) / self._num_classes
        self._current_left = numpy.ones(
            (self._num_samples, self._num_classes)) / self._num_classes
        self._build_average_estimates()

    def estimates(self):
        """Returns current estimates for given data."""
        almost = [self._current_left[one] + self._current_right[one]
                  for one in range(self._num_samples)]
        # Normalization is not needed when only looking for max value
        return almost
        # return [almost[one] / sum(almost[one])
        #         for one in range(self._num_samples)]

    def _inner_loop(self):
        """One iteration of the algorithm, to be extended."""
        self._iterate_right()
        self._iterate_left()

    def step(self, num_steps=1):
        """Performs given number of iterations of the algorithm."""
        final_iteration = self._current_iteration + num_steps
        while self._current_iteration < final_iteration:
            self._current_iteration += 1
            logging.debug('Starting iteration %d', self._current_iteration)
            self._inner_loop()
