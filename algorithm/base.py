# coding=utf-8
"""Methods shared by all transfer learning algorithms."""
import abc


class Algorithm(abc.ABC):
    """Base abstract algorithm class."""

    @abc.abstractmethod
    def start(self, labeled_labels, source_models, cluster_models):
        """Configure algorithm class with global variables."""

    @abc.abstractmethod
    def estimates(self):
        """Return current estimates by the algorithm for input data."""

    @abc.abstractmethod
    def step(self, num_steps=1):
        """Performs one iteration of the algorithm."""
