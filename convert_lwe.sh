#!/bin/bash
# Run pipeline to get input data for LWE datasets
set -e

FILE=$1

CLUTO='/home/cserpell/Downloads/cluto-2.1.1/Linux/vcluster'
BBR_TRAIN='/home/cserpell/Downloads/bbr/BBRtrain'
BBR_CLASSIFY='/home/cserpell/Downloads/bbr/BBRclassify'
SVM_TRAIN='/home/cserpell/Downloads/libsvm-3.22/svm-train'
SVM_PREDICT='/home/cserpell/Downloads/libsvm-3.22/svm-predict'
SNOW='/home/cserpell/Downloads/Snow_v3.2/snow'

CLUTO_INPUT="${FILE}.cluto"
BBR_INPUT="${FILE}.bbrinput"
SNOW_INPUT="${FILE}.snowinput"

echo "Preparing CLUTO input ${CLUTO_INPUT}"
CLUTO_TMP="${CLUTO_INPUT}.tmp"
awk -F' ' 'BEGIN { maxf = 0; numrows = 0; nonzero = 0; } { numrows = numrows + 1; for (i = 2; i <= NF; i++) { split($i, two, ":"); if(i != 2) { printf(" "); } if (two[1] > maxf) { maxf = two[1]; } nonzero = nonzero + 1; printf("%s %s", two[1], two[2]); } printf("\n"); } END { printf("\n%d %d %d\n", numrows, maxf, nonzero); }' "${FILE}" > "${CLUTO_TMP}"
tail -n 1 "${CLUTO_TMP}" | cat - "${CLUTO_TMP}" | head -n -1 > "${CLUTO_INPUT}"
rm "${CLUTO_TMP}"

echo "Preparing BBR input ${BBR_INPUT}"
awk -F' ' '{ if ($1 == "0") { printf("-1"); } else { printf("%s", $1); } for (i = 2; i <= NF; i++) { printf(" %s", $i); } printf("\n"); }' "${FILE}" > "${BBR_INPUT}"

echo "Preparing SNoW input ${SNOW_INPUT}"
awk -F' ' '{ printf("%s", $1); for (i = 2; i <= NF; i++) { split($i, two, ":"); printf(", %s(%f)", two[1] + 1, two[2]); } printf(":\n"); }' "${FILE}" > "${SNOW_INPUT}"

echo 'Running CLUTO clustering'
$CLUTO "${CLUTO_INPUT}" 2

if [[ $FILE = *'train'* ]]; then
  TEST=$(echo "${FILE}" | sed 's/train/test/')
  BBR_MODEL="${FILE}.bbrmodel"
  SNOW_MODEL="${FILE}.snowmodel"
  SVM_MODEL="${FILE}.svmmodel"
  BBR_TEST_RESULTS="${TEST}.bbrresults"
  SNOW_TEST_RESULTS="${TEST}.snowresults"
  SVM_TEST_RESULTS="${TEST}.svmresults"
  BBR_TEST_INPUT="${TEST}.bbrinput"
  SNOW_TEST_INPUT="${TEST}.snowinput"
  echo 'Training BBR'
  $BBR_TRAIN "${BBR_INPUT}" "${BBR_MODEL}"
  echo "Running BBR on test ${TEST}"
  $BBR_CLASSIFY -r "${BBR_TEST_RESULTS}" "${BBR_TEST_INPUT}" "${BBR_MODEL}"
  echo 'Training SVM'
  $SVM_TRAIN "${FILE}" "${SVM_MODEL}"
  echo "Running SVM on test ${TEST}"
  $SVM_PREDICT "${TEST}" "${SVM_MODEL}" "${SVM_TEST_RESULTS}"
  echo 'Training and running SNoW on test'
  $SNOW -train -I "${SNOW_INPUT}" -F "${SNOW_MODEL}" -S 3 -r 5 -T "${SNOW_TEST_INPUT}" -R "${SNOW_TEST_RESULTS}" -o allboth
fi

