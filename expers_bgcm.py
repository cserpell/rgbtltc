# coding=utf-8
"""Transfer learning experiment."""
import argparse
import logging
import os
import random
import sys

import numpy
import scipy
import sklearn.linear_model

from algorithm import oac3
from algorithm import regularized_oac3

# Experiments
_BGCM = 'bgcm'
_LWE = 'lwe'

# Available algorithms
_OAC3 = 'oac3'
_REGULARIZED_OAC3 = 'regularized_oac3'

# Default options
_DEFAULT_EXPERIMENT = _BGCM
_DEFAULT_ALGORITHM = _OAC3
_DEFAULT_MAX_ITER = 20
_DEFAULT_PERCENTAGE_LABEL = 5.0
_DEFAULT_REPETITIONS = 5


class Experiment(object):
    """Main class with BGCM transfer learning experiment."""

    def __init__(self, algorithm_name, data_dir, alpha, beta, lambd, step,
                 max_iter, divergence, percentage_label, repetitions,
                 experiment, cross_validation):
        """Constructor of Experiment.
        @param algorithm_name: string name of algorithm to use in experiment
        @param data_dir: string name of directory with data
        @param alpha: float parameter for algorithms that use it
        @param beta: float parameter for algorithms that use it
        @param lambd: float parameter for algorithms that use it
        @param step: float parameter for algorithms that use it
        @param max_iter: int maximum number of iterations for algorithms
        @param divergence: string kind of divergence for OAC3 algorithm
        @param percentage_label: float percentage of labeled data
        @param repetitions: int number of repetitions per task
        @param experiment:string kind of experiment data to read
        @param cross_validation: whether to use random subsamples for each rep.
        """
        self._algorithm = algorithm_name
        self._data_dir = data_dir
        self._alpha = alpha
        self._beta = beta
        self._lambda = lambd
        self._step = step
        self._max_iter = max_iter
        self._divergence = divergence
        self._percentage_label = percentage_label
        self._repetitions = repetitions
        self._num_tasks = 6
        self._experiment = experiment
        self._cross_validation = cross_validation

    def __build_algorithm(self, num_classes):
        """Returns correct algorithm instance from config."""
        if self._algorithm == _OAC3:
            return oac3.OAC3(
                num_classes, alpha=self._alpha, beta=self._beta,
                lambd=self._lambda, divergence=self._divergence)
        if self._algorithm == _REGULARIZED_OAC3:
            return regularized_oac3.RegularizedOAC3(
                num_classes, alpha=self._alpha, beta=self._beta,
                lambd=self._lambda, step=self._step,
                divergence=self._divergence)
        return None

    def _read_data_bgcm(self, task_num):
        """Read data from configured directory in BGCM format."""
        file_name = os.path.join(self._data_dir,
                                 'mbase{:01d}.csv'.format(task_num))
        source_models = []
        cluster_models = []
        with open(file_name) as the_file:
            for line in the_file:
                numbers = line.split(',')
                # int(...) below to make sure it is an int
                source_models.append(
                    (int(numbers[0]) - 1, int(numbers[1]) - 1))
                cluster_models.append(
                    (int(numbers[2]) - 1, int(numbers[3]) - 1))
        num_classes = self._get_num_classes(cluster_models)
        final_source_models = []
        for one in source_models:
            models = []
            for model in one:
                models.append(numpy.eye(1, num_classes, model))
            final_source_models.append(models)
        file_name = os.path.join(self._data_dir,
                                 'label{:01d}.csv'.format(task_num))
        labels = []
        with open(file_name) as the_file:
            for line in the_file:
                labels.append(int(line) - 1)
        return final_source_models, cluster_models, labels, num_classes

    def _read_data_lwe(self, task_num):
        """Read data from configured directory in LWE format."""
        file_name = os.path.join(self._data_dir,
                                 'test{:01d}.svm.ready'.format(task_num))
        source_models = []
        cluster_models = []
        labels = []
        with open(file_name) as the_file:
            for line in the_file:
                numbers = line.strip().split(' ')
                # int(...) or float(...) below to make sure it is a number
                labels.append(int(numbers[0]))
                clust = int(numbers[1])
                cluster_models.append((clust if clust != -1 else 0,))
                source0 = numpy.array((float(numbers[3]), float(numbers[4])))
                source1 = numpy.array((float(numbers[6]), float(numbers[7])))
                source2 = numpy.array((float(numbers[9]), float(numbers[10])))
                source_models.append((source0, source1, source2))
        num_classes = self._get_num_classes(cluster_models)
        return source_models, cluster_models, labels, num_classes

    @staticmethod
    def _get_num_classes(cluster_models):
        max_class = 0
        for one in cluster_models:
            for model_class in one:
                if model_class > max_class:
                    max_class = model_class
        return max_class + 1

    def run(self):
        """Main execution point."""
        print('data,algorithm,divergence,repetition,iteration,alpha,lambda,'
              'step,task,accuracy,precision1,recall1,total1,'
              'precision2,recall2,total2,precision3,recall3,total3,'
              'precision4,recall4,total4,')
        for task in range(1, self._num_tasks + 1):
            source_models, cluster_models, labels, num_classes = (
                self._read_data_bgcm(task) if self._experiment == _BGCM else
                self._read_data_lwe(task))
            for repetition in range(1, self._repetitions + 1):
                logging.debug('Starting repetition %d', repetition)
                if self._cross_validation:
                    # Leave 10% of data out
                    logging.debug('Cross validation is active')
                    num_data = len(source_models)
                    selection = numpy.random.choice(
                        num_data, int(0.9 * num_data), replace=False)
                    red_source_models = []
                    red_cluster_models = []
                    red_labels = []
                    for one_sel in selection:
                        red_source_models.append(source_models[one_sel])
                        red_cluster_models.append(cluster_models[one_sel])
                        red_labels.append(labels[one_sel])
                else:
                    red_source_models = source_models
                    red_cluster_models = cluster_models
                    red_labels = labels
                labeled_sources = []
                labeled_clusters = []
                labeled_labels = []
                unlabeled_sources = []
                unlabeled_clusters = []
                unlabeled_labels = []
                for one, value in enumerate(red_source_models):
                    if random.uniform(0, 100) < self._percentage_label:
                        labeled_sources.append(value)
                        labeled_clusters.append(red_cluster_models[one])
                        labeled_labels.append(red_labels[one])
                    else:
                        unlabeled_sources.append(value)
                        unlabeled_clusters.append(red_cluster_models[one])
                        unlabeled_labels.append(red_labels[one])
                new_sources = (scipy.vstack((labeled_sources,
                                             unlabeled_sources))
                               if labeled_sources else unlabeled_sources)
                new_clusters = (scipy.vstack((labeled_clusters,
                                              unlabeled_clusters))
                                if labeled_clusters else unlabeled_clusters)
                fixed_labels = labeled_labels + unlabeled_labels
                model = self.__build_algorithm(num_classes)
                model.start(labeled_labels, new_sources, new_clusters)
                logging.debug('Fitting model')
                for one_iter in range(self._max_iter):
                    model.step()
                    pred = model.estimates()
                    max_pred = [numpy.argmax(pred[one])
                                for one in range(len(pred))]
                    confusion = sklearn.metrics.confusion_matrix(fixed_labels,
                                                                 max_pred)
                    last_score = []
                    for one_class in range(len(confusion)):
                        # Expected number of items in the class
                        expected = sum(confusion[one_class])
                        # Total classified as this class
                        total_class = sum(confusion[row][one_class]
                                          for row in range(len(confusion)))
                        diagonal = float(confusion[one_class][one_class])
                        precision_class = (1.0 if total_class == 0 else
                                           diagonal / total_class)
                        recall_class = (1.0 if expected == 0 else
                                        diagonal / expected)
                        last_score.append(
                            (precision_class, recall_class, expected))
                    accuracy = (sum(score[1] * score[2]
                                    for score in last_score) /
                                sum(score[2] for score in last_score))
                    scores = ','.join(','.join(
                        str(value) for value in one) for one in last_score)
                    print('{},{},{},{},{},{},{},{},{},{},{}'.format(
                        self._data_dir, self._algorithm, self._divergence,
                        repetition, one_iter + 1, self._alpha,
                        self._lambda, self._step, task, accuracy, scores))


def main():
    """Process command line arguments and run program."""
    parser = argparse.ArgumentParser(description='Comparisons with BGCM.')
    parser.add_argument('--cross_validation', action='store_const', const=True,
                        help='when active, each repetition has random sample')
    parser.add_argument('--experiment', type=str, default=_DEFAULT_EXPERIMENT,
                        choices=[_BGCM, _LWE], help='experiment data to run')
    parser.add_argument('--algorithm', type=str, default=_DEFAULT_ALGORITHM,
                        choices=[_OAC3, _REGULARIZED_OAC3],
                        help='algorithm used to learn')
    parser.add_argument('--data_dir', type=str, default='.',
                        help='path to read data files from')
    parser.add_argument('--debug', action='store_const', const=True,
                        help='when active, it writes debug info')
    parser.add_argument('--alpha', type=float, default=oac3.DEFAULT_ALPHA,
                        help='Alpha parameter for algorithms that use it')
    parser.add_argument('--beta', type=float, default=oac3.DEFAULT_BETA,
                        help='Beta parameter for algorithms that use it')
    parser.add_argument('--lambd', type=float, default=oac3.DEFAULT_LAMBDA,
                        help='Lambda parameter for algorithms that use it')
    parser.add_argument('--step', type=float,
                        default=regularized_oac3.DEFAULT_STEP,
                        help='Step parameter for algorithms that use it')
    parser.add_argument('--max_iter', type=int, default=_DEFAULT_MAX_ITER,
                        help='Maximum number of iterations for algorithm')
    parser.add_argument('--divergence', type=str,
                        default=oac3.DEFAULT_DIVERGENCE,
                        choices=[oac3.SQUARED_EUCLIDEAN, oac3.GENERALIZED_I],
                        help='Divergence for OAC3')
    parser.add_argument('--percentage_label', type=float,
                        default=_DEFAULT_PERCENTAGE_LABEL,
                        help='Percentage of labeled data')
    parser.add_argument('--repetitions', type=int,
                        default=_DEFAULT_REPETITIONS,
                        help='Number of repetitions for each task')
    args = parser.parse_args()
    level = logging.DEBUG if args.debug else logging.INFO
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                        level=level, stream=sys.stderr)
    Experiment(args.algorithm, args.data_dir, args.alpha, args.beta,
               args.lambd, args.step, args.max_iter, args.divergence,
               args.percentage_label, args.repetitions, args.experiment,
               args.cross_validation).run()


if __name__ == "__main__":
    main()
