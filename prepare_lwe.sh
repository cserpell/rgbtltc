#!/bin/bash
# Prepare input for LWE experiments from source classifiers and clusters
set -e

FILE=$1

CLUTO_INPUT="${FILE}.cluto.clustering.2"
BBR_INPUT="${FILE}.bbrresults"
SNOW_INPUT="${FILE}.snowresults"
SVM_INPUT="${FILE}.svmresults"

FILE_OUTPUT="${FILE}.ready"

echo "Parsing BBR results ${BBR_INPUT}"
BBR_TMP="${BBR_INPUT}.tmp"
awk -F' ' '{ prob = $1; label = $2; if (label == "-1") { label = "0"; } printf("%s %f %f\n", label, 1.0 - prob, prob); }' "${BBR_INPUT}" > "${BBR_TMP}"

echo "Parsing SVM results ${SVM_INPUT}"
SVM_TMP="${SVM_INPUT}.tmp"
awk -F' ' '{ label = $1; prob = 1.0; if (label == "0") { prob = 0.0; } printf("%s %f %f\n", label, 1.0 - prob, prob); }' "${SVM_INPUT}" > "${SVM_TMP}"

echo "Parsing SNoW results ${SNOW_INPUT}"
SNOW_TMP="${SNOW_INPUT}.tmp"
awk -F' ' 'BEGIN { parsing = 0; } { if (parsing == 1) { label0 = substr($1, 1, length($1) - 1); prob0 = $5; if (substr(prob0, length(prob0), 1) == "*") { prob0 = substr(prob0, 1, length(prob0) - 1); } parsing = 2; } else if (parsing == 2) { label1 = substr($1, 1, length($1) - 1); prob1 = $5; if (substr(prob1, length(prob1), 1) == "*") { prob1 = substr(prob1, 1, length(prob1) - 1); } parsing = 0; label = label0; prob = prob0; if (label0 == "0") { prob = prob1; } printf("%s %f %f\n", label, 1.0 - prob, prob); } if ($1 == "Example") { parsing = 1; } }' "${SNOW_INPUT}" > "${SNOW_TMP}"

echo "Joining all results in ${FILE_OUTPUT}"
cut -d " " -f1,1 "${FILE}" | paste -d " " - "${CLUTO_INPUT}" "${BBR_TMP}" "${SVM_TMP}" "${SNOW_TMP}" > ${FILE_OUTPUT}
