#!/bin/bash
# Script to process one output file, averaging repetitions

# Exit immediately
set -e

REPETITIONS=5
ITERATIONS=100

AWK_SCRIPT=$(cat << EOM
BEGIN {
  max_nf = 0;
}
{
  if (\$5 == ${ITERATIONS}) {
    the_index = \$1 "," \$2 "," \$3 "," \$6 "," \$7 "," \$8 "," \$9;
    crep = 1;
    if (the_index in repetitions) {
      crep = repetitions[the_index] + 1;
    }
    repetitions[the_index] = crep;
    for (i = 10; i <= NF; i++) {
      cave = 0.0;
      if ((the_index, i) in averages) {
        cave = averages[the_index, i];
      }
      averages[the_index, i] = ((crep - 1) * cave + \$i) / crep;
      variances[the_index, i] = (variances[the_index, i] * crep + (cave - \$i) * (cave - \$i)) * (crep - 1) / crep / crep;
    }
    if (NF > max_nf) {
      max_nf = NF;
    }
  }
}
END {
  for (the_index in repetitions) {
    printf("%s,%d,", the_index, repetitions[the_index]);
    for (i = 10; i < max_nf; i++) {
      printf("%f,%f,", averages[the_index, i], variances[the_index, i]);
    }
    printf("%f,%f\n", averages[the_index, max_nf], variances[the_index, max_nf]);
  }
}
EOM
)

AWK_SCRIPT2=$(cat << EOM2
BEGIN {
  started = 0;
  lalg = "";
  ldiv = "";
  ldat = "";
  ltas = "";
}
{
  if (lalg != \$2 || ldiv != \$3 || ldat != \$1 || ltas != \$7) {
    if (started == 1) {
      printf("%s,%s,%s,%s,%d,%f,%f,%f,%f\n", lalg, ldiv, ldat, ltas, mrep, mave, mvar, 100 * mave, sqrt(10000 * mvar * mrep / (mrep - 1)));
    }
    mave = 0.0;
    mvar = 0.0;
    mrep = 0;
    lalg = \$2;
    ldiv = \$3;
    ldat = \$1;
    ltas = \$7;
    started = 1;
  }
  if (\$9 > mave) {
    mrep = \$8
    mave = \$9;
    mvar = \$10;
  }
}
END {
  if (started == 1) {
    printf("%s,%s,%s,%s,%d,%f,%f,%f,%f\n", lalg, ldiv, ldat, ltas, mrep, mave, mvar, 100 * mave, sqrt(10000 * mvar * mrep / (mrep - 1)));
  }
}
EOM2
)

grep -v anaconda | \
  grep -v repetition | \
  grep -v INICIO | \
  grep -v DIRECTORY | \
  awk -F, "${AWK_SCRIPT}" > aaaa.csv

cat aaaa.csv | \
  sort -t, -k7,7 | \
  awk -F, "${AWK_SCRIPT2}"
